<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @param UserRepository $userRepository
     * @param Session $session
     * @return Response
     */
    public function index(UserRepository $userRepository, Session $session): Response
    {
        $user = $this->getUser();
        if(!$user){
          $session->set("message", "Merci de vous connecter");
          return $this->redirectToRoute('app_login');
        }
        elseif (in_array('ROLE_ADMIN', $user->getRoles())){
            return $this->render('user/index.html.twig',[
                'users' => $userRepository->findAll()
            ]);
        }
        return  $this->redirectToRoute('member');
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Session $session
     * @return Response
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder, Session $session): Response
    {
        $user = $this->getUser();
        if($user){
            $session->set("message", "Vous ne pouvez pas créer un compte lorsque vous étes connecté");
            return $this->redirectToRoute('member');
        }
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            //crypt password
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $user->getPassword())
            );
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Session $session
     * @param $id
     * @return Response
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface  $passwordEncoder, Session $session, $id): Response
    {
        if($user->getId() != $id) {
            $session->set("message", "Vous ne pouvez pas modifier cet utilisateur");
            return $this->redirectToRoute('member');
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // edit password
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @param Session $session
     * @param $id
     * @return Response
     */
    public function delete(Request $request, User $user, Session $session, $id): Response
    {

        if($user->getId() != $id) {
            $session->set("message", "Vous ne pouvez pas supprimer cet utilisateur");
            return $this->redirectToRoute('member');
        }
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
            $session = new Session();
            $session->invalidate();
        }

        return $this->redirectToRoute('home');
    }
}
