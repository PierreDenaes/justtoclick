<?php

namespace App\Controller;

use App\Repository\ReasonRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class NavController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param Session $session
     * @return Response
     */
    public function home(Session $session)
    {
        $return = [];
        if($session->has('message')){
            $message =$session->get('message');
            $session->remove('message');
            $return['message'] = $message;
        }
        return $this->render('nav/home.html.twig', $return);
    }

    /**
     * @Route("/member", name="member")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Session $session
     * @param UserRepository $userRepository
     * @param ReasonRepository $reasonRepository
     * @return Response
     */
    public function member(Session $session, UserRepository $userRepository, ReasonRepository $reasonRepository)
    {
        $return = [];
        if($session->has('message')){
            $message =$session->get('message');
            $session->remove('message');
            $return['message'] = $message;
        }
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('nav/member.html.twig', ['users' => $userRepository->findAll(), 'reasons' => $reasonRepository->findAll()]);
    }

    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     * @param Session $session
     * @return RedirectResponse|Response
     */
    public function admin(Session $session)
    {
        $user = $this->getUser();
        if(!$user)
        {
            $session->set("message", "Merci de vous connecter");
            return $this->redirectToRoute('app_login');
        }

        else if(in_array('ROLE_ADMIN', $user->getRoles())){
            return $this->render('nav/admin.html.twig');
        }
        $session->set("message", "Vous n'avez pas le droit d'acceder à la page admin vous avez été redirigé sur cette page");
        if($session->has('message'))
        {
            $message = $session->get('message');
            $session->remove('message'); //on vide la variable message dans la session
            $return['message'] = $message; //on ajoute à l'array de paramètres notre message
        }
        return $this->redirectToRoute('home', $return);

    }

}